let voices =[];
speechSynthesis.addEventListener('voiceschanged',function(){
    voices = speechSynthesis.getVoices();
})





// Raccolgo alla pagina gli elemnti
const textArea = document.querySelector('textarea')
const playButton = document.querySelector('button')
const pitchBAr = document.querySelector('input')
const duckFigure = document.querySelector('figure')




// osservazione tasto play
playButton.addEventListener("click",function(){
    const textLength = textArea.value.trim().length;
     
    if(textLength > 0 ){
        talk();
    }

})

// funzione per parlare la paperella
function talk(){
    const text = textArea.value;
    const pitch = pitchBAr.value;


//  utterance.volume= 1;
//  utterance.rate= 1;
    

     const utterance = new SpeechSynthesisUtterance(text);

     utterance.pitch= pitch;

     const femaleVoice= voices.find(function(voice){
        if(voice.name.includes('Elsa')){
            return true;
        }
     });

     utterance.voice = femaleVoice;

     speechSynthesis.speak(utterance);
    
    utterance.addEventListener('start', function(){
        textArea.disabled =true;
        pitchBAr.disabled = true;
        playButton.disabled =true;

        duckFigure.classList.add('talking');

    })

    utterance.addEventListener('end', function(){
        duckFigure.classList.remove('talking')
    })




}

